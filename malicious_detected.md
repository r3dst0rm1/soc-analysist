**Malicous IP, Code, Hash ...**
<details><summary>+</summary>

1. [app.any.run ](https://app.any.run/tasks/a66178de-7596-4a05-945d-704dbf6b3b90)

2. [virustotal](https://www.virustotal.com/gui/)

3. [fast-flux](https://unit42.paloaltonetworks.com/fast-flux-101/)

4. [bazaar](https://bazaar.abuse.ch/)

5. [malshare](https://malshare.com/)

6. [socprime](https://tdm.socprime.com/)

7. [ssdeep ](https://ssdeep-project.github.io/ssdeep/index.html) --> this is usefull to detecte when attacker make some change in the malware source Code

</details>

**Network artifact**
<details><summary>+</summary>
# tshark

TShark to filter out the User-Agent strings

`tshark --Y http.request -T fields -e http.host -e http.user_agent -r analysis_file.pcap `

[tshark cheat cheet ](https://gist.github.com/githubfoam/6c9e07f95c2eb03ec4ae9709252c713f)

</details>
**Misc**
 <details><summary+</summary>
1. To identify original web site from short url and **+** at end for short url
</details>


